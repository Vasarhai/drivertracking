package com.myapp.drivertracking;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public String stringUrl = "http://csrv-devtaxofon02.modera.ee:5000/";
    public JSONObject resultJSON;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private MapFragment mapFragment = new MapFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            MainActivityFragment firstFragment = new MainActivityFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    protected void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {

            Log.d("Latitude", String.valueOf(mLastLocation.getLatitude()));
            Log.d("Longitude", String.valueOf(mLastLocation.getLongitude()));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void ActivateMap() {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, mapFragment);

        transaction.commit();

        SupportMapFragment mapFragment2 = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment2 != null) {
            mapFragment2.getMapAsync(this);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (mLastLocation != null) {

            LatLng currPos = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mMap.addMarker(new MarkerOptions().position(currPos).title("I'm here"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currPos));
        }
    }

    public void loginAction(View view) {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

            new DownloadWebpageTask().execute(GetFormInfo());
        } else {
            Toast.makeText(MainActivity.this, "No network connection available", Toast.LENGTH_SHORT).show();
        }
    }

    private JSONObject GetFormInfo() {
        String user = ((EditText) findViewById(R.id.editText)).getText().toString();
        String pass = ((EditText) findViewById(R.id.editText2)).getText().toString();
        String car = ((EditText) findViewById(R.id.editText3)).getText().toString();

        //Create JSONObject here
        JSONObject jsonParam = new JSONObject();
        JSONObject jsonParam2 = new JSONObject();
        try {
            jsonParam.put("action", "login");

            jsonParam2.put("username", user);
            jsonParam2.put("password", pass);
            jsonParam2.put("car_number", car);

            jsonParam.put("data", jsonParam2);
        } catch (Exception e) {

        }
        return jsonParam;
    }

    public void ConnectSocket() {
        if (resultJSON == null) {
            Log.e("ERROR", "Can't get car data. Make sure you are logged in.");
            return;
        }
        IO.Options opts = new IO.Options();
        opts.transports = new String[]{"polling"};
        try {
            JSONObject data = new JSONObject(resultJSON.getString("data"));
            Log.d("11", data.toString());
            opts.query = "car_id=" + data.getLong("car_id") + "&driver_id=" + data.getLong("driver_id") + "&uid=" + data.getString("uid");
        } catch (JSONException e) {
            Log.e("ERROR", "Can't parse JSON." + e);
            return;
        }
        final Socket socket;
        try {
            socket = IO.socket(stringUrl, opts);
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {

                    float latitude = 12.12f;
                    float longitude = 22.22f;

                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("latitude", latitude);
                        obj.put("longitude", longitude);
                        obj.put("status_car", "READY");
                        socket.emit("location", obj);
                    } catch (JSONException e) {

                    }

                }

            }).on("response", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject) args[0];
                    Log.d("location", obj.toString());
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }
            });
            socket.connect();
        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private class DownloadWebpageTask extends AsyncTask<JSONObject, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(JSONObject... urls) {

            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                Log.e("ERROR", "Can't retrieve data");
                return null;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(JSONObject result) {

            boolean isConnected = false;
            try {
                resultJSON = result;
                Toast.makeText(MainActivity.this, result.toString(), Toast.LENGTH_SHORT).show();
                Log.d("RESULT", result.toString());
                if (result.getString("status").equals("true"))
                    isConnected = true;
            } catch (Exception e) {

            }
            if (isConnected) {
                // connection succeeded
                Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();

                ActivateMap();

                ConnectSocket();
            } else {
                Toast.makeText(MainActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
            }
        }


        private JSONObject downloadUrl(JSONObject jsonParam) throws IOException {

            String contentAsString;
            InputStream is = null;

            try {
                URL url = new URL(stringUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("App-Version", "0.0.1");
                conn.setRequestProperty("App-Type", "driver");
                conn.setRequestProperty("Os-Name", "android");
                // Starts the query
                conn.connect();

                // Send POST output.
                DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                out.write(jsonParam.toString().getBytes("UTF-8"));
                out.flush();
                out.close();

                int response = conn.getResponseCode();
                Log.d("CODE", "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                contentAsString = convertStreamToString(is);

                // parsing into JSON
                try {
                    JSONObject obj = new JSONObject(contentAsString);
                    Log.d("My App", obj.toString());
                    return obj;

                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + contentAsString + "\"");
                }
                return null;

                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } catch (Exception e) {
                Log.d("Exception", "Error: " + e);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
            return null;
        }

        private String convertStreamToString(InputStream is) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }
    }
}
